---
layout: post
title:  "My first Jekyll post"
date:   2018-03-18 12:54:14 +0200
categories: jekyll welcome
---
Questo è il mio primo posto fatto su `GitLab` con il framework [Jekyll][jekyll-docs].

Ho seguito l'esempio di un post già scritto... vediamo se funziona!

Ri-pusho perché alla prima non ha funzionato!

Jekyll also offers powerful support for code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
